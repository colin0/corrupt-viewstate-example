public class Comp1Controller {
    public Integer Int1 {get;set;} { Int1 = 0; }
    public String Param1 {get;set;} { Param1 = 'INIT'; }
    public String Str1 {get;set;} { Str1 = 'str1'; }
    
    public void action1() {
        Int1--;
        Str1 += ' • ' + Param1;
    }
}