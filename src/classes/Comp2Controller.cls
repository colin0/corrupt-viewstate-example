public class Comp2Controller {
    public Integer Int2 {get;set;} { Int2 = 0; }
    public String Param2 {get;set;} { Param2 = 'init'; }
    public String Str2 {get;set;} { Str2 = 'str2'; }
    
    public void action2() {
        Int2++;
        Str2 += ' | ' + Param2;
    }
}